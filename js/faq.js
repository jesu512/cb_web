$(function ()
{
    $('#o_pregunta').on('show.bs.modal', function (e){
      console.log('El componente modal se esta abriendo');
      $('#btnPregunta').removeClass('btn-outline-success');
      $('#btnPregunta').addClass('btn-danger');
      $('#btnPregunta').prop('disabled', true);
    });
    $('#o_pregunta').on('shown.bs.modal', function (e){
      console.log('El componente modal se abrió');
    });
    $('#o_pregunta').on('hide.bs.modal', function (e){
      console.log('El componente modal se esta ocultando');
    });
    $('#o_pregunta').on('hidden.bs.modal', function (e){
      console.log('El componente modal se ocultó');
      $('#btnPregunta').removeClass('btn-danger');
      $('#btnPregunta').addClass('btn-outline-success');
      $('#btnPregunta').prop('disabled', false);
  });
});
